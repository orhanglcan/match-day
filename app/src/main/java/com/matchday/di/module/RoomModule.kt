package com.matchapp.di.module

import android.app.Application
import com.matchapp.data.local.AppDatabase
import com.matchday.data.local.dao.MatchDao
import com.matchday.data.local.dao.TeamDao
import com.matchday.data.local.repository.MatchRepository
import com.matchday.data.local.repository.TeamRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class RoomModule(application: Application) {

    var appDatabase: AppDatabase = AppDatabase.getInstance(application.applicationContext)

    @Singleton
    @Provides
    fun providesAppDatabase(): AppDatabase {
        return appDatabase
    }

    @Singleton
    @Provides
    fun providesTeamRepository(appDatabase: AppDatabase): TeamDao {
        return appDatabase.teamDao()
    }

    @Singleton
    @Provides
    fun providesMatchDao(appDatabase: AppDatabase): MatchDao {
        return appDatabase.matchDao()
    }

    //repositories

    @Singleton
    @Provides
    fun matchRepository(matchDao: MatchDao): MatchRepository {
        return MatchRepository(matchDao)
    }

    @Singleton
    @Provides
    fun teamRepository(teamDao: TeamDao): TeamRepository {
        return TeamRepository(teamDao)
    }

}