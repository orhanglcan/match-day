package com.matchapp.di.component

import android.app.Activity
import android.app.Application
import com.matchapp.data.local.AppDatabase
import com.matchapp.di.module.AppModule
import com.matchapp.di.module.RoomModule
import com.matchapp.ui.bases.BaseCompactActivity
import com.matchapp.ui.bases.BaseCompactFragment
import com.matchday.data.local.dao.MatchDao
import com.matchday.data.local.dao.TeamDao
import com.matchday.data.local.repository.MatchRepository
import com.matchday.data.local.repository.TeamRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [],
    modules = [AppModule::class, RoomModule::class]
)
interface AppComponent {

    //fun <T:ViewDataBinding,V:BaseViewModel<*>>inject(baseActivity: BaseActivity<T,V>)
    //fun <T : ViewDataBinding?, V : BaseViewModel<*>?> inject(baseActivity: BaseActivity<T, V>)
    fun inject(baseCompatActivity: Activity)
    fun inject(baseCompatActivity: BaseCompactActivity)
    fun inject(baseCompactFragment: BaseCompactFragment)
    //fun inject(splashActivity: SplashActivity)

    fun application(): Application
    fun appDatabase(): AppDatabase

    fun matchDao(): MatchDao
    fun teamDao(): TeamDao

    fun matchRepository(): MatchRepository
    fun teamRepository(): TeamRepository

}