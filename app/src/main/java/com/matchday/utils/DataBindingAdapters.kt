package com.matchday.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.matchday.R
import com.matchday.utils.manager.ImageManager

@BindingAdapter(value = ["loadImage"], requireAll = false)
fun convertImage(imageView: ImageView, url: String?) {
    if (url == null) imageView.setImageResource(R.drawable.ic_launcher_foreground)
    else {
        ImageManager.instance()?.showImage(url.toString(), imageView)
    }
}
