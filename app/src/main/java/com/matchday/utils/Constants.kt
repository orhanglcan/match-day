package com.matchapp.utils

import android.graphics.drawable.GradientDrawable

class Constants {

    companion object {
        var PACKAGE = "com.matchapp"
        var USER_AGENT = ""
        var TOKEN = ""
        var APP_NAME = "Match App"
        var APP_TAG = "MatchApp"
        var DEVICE_ID = ""
        var BASE_URL = "https://api-football-v1.p.rapidapi.com/v2/teams/"
        var RAPID_API_KEY = "74991b0f59mshf0d0b73631f1de3p1606a2jsnef545eeb0825"
        var RAPID_API_HOST = "api-football-v1.p.rapidapi.com"

    }
}