package com.matchapp.utils.manager

import android.content.Context
import android.content.SharedPreferences
import com.matchapp.utils.Constants.Companion.PACKAGE
import com.matchday.MatchApp

class CacheManager {

    private val sharedPrefs: SharedPreferences = MatchApp.instance!!.applicationContext.getSharedPreferences(PACKAGE, Context.MODE_PRIVATE)

    fun putString(key: String?, string: String?): Boolean {
        val editor = sharedPrefs.edit()
        editor.putString(key, string)
        editor.apply()
        return true
    }

    fun putToken(string: String?): Boolean {
        val editor = sharedPrefs.edit()
        editor.putString("token", string)
        editor.apply()
        return true
    }

    fun putRemoteConfig(string: String?): Boolean {
        val editor = sharedPrefs.edit()
        if (string != null) {
            editor.putString("remoteConfig", string)
            editor.apply()
        }
        return true
    }

    fun putInteger(key: String?, integer: Int?): Boolean {
        val editor = sharedPrefs.edit()
        editor.putInt(key, integer!!)
        editor.apply()
        return true
    }

    fun putBoolean(key: String?, value: Boolean): Boolean {
        val editor = sharedPrefs.edit()
        editor.putBoolean(key, value)
        editor.apply()
        return true
    }

    fun putLong(key: String?, value: Long): Boolean {
        val editor = sharedPrefs.edit()
        editor.putLong(key, value)
        editor.apply()
        return true
    }

    fun getStringFromSP(key: String?, defaultval: String?): String? {
        return sharedPrefs.getString(key, defaultval)
    }

    fun getTokenFromSP(defaultval: String?): String? {
        return sharedPrefs.getString("token", defaultval)
    }

    fun getIntegerFromSP(key: String?, defaultval: Int): Int {
        return sharedPrefs.getInt(key, defaultval)
    }

    fun getBooleanFromSP(key: String?, defaultval: Boolean): Boolean {
        return sharedPrefs.getBoolean(key, defaultval)
    }

    fun getLongFromSP(key: String?, defaultval: Long): Long {
        return sharedPrefs.getLong(key, defaultval)
    }

    fun fullClearSP() {
        sharedPrefs.edit().clear().apply()
    }

    companion object {
        var instance: CacheManager? = null
            get() {
                if (field == null) field =
                    CacheManager()
                return field
            }
            private set
    }

}