package com.matchday.utils.manager

import android.content.Context
import android.widget.ImageView
import com.matchapp.utils.manager.LogManager.Companion.logD
import com.matchday.R
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso

class ImageManager {

    companion object {
        private var instance: ImageManager? = null

        fun instance(): ImageManager? {
            if (instance == null) {
                instance = ImageManager()
                return instance
            }

            return instance
        }
    }

    fun init(context: Context) {
        try {
            val builder = Picasso.Builder(context)
            builder.downloader(OkHttp3Downloader(context, Long.MAX_VALUE))
            val built = builder.build()
            //built.setIndicatorsEnabled(true)
            built.isLoggingEnabled = true
            Picasso.setSingletonInstance(built)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showImage(uri: String, view: ImageView) {
        logD("ImageUrl", uri)
        Picasso.get()
            .load(uri)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .into(view,object:Callback{
                override fun onSuccess() {
                }

                override fun onError(e: java.lang.Exception?) {
                    Picasso.get()
                        .load(uri)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(view)
                }

            })
    }

    fun showImage(uri: Int, view: ImageView) {
        logD("ImageUrl", uri.toString())
        Picasso.get()
            .load(uri)
            //.networkPolicy(NetworkPolicy.OFFLINE)
            .into(view)
    }
}