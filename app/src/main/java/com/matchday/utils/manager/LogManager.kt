package com.matchapp.utils.manager

import android.util.Log
import com.matchday.BuildConfig

class LogManager {
    companion object {
        fun logD(tag: String, value: String) {
            if (BuildConfig.DEBUG)
                Log.d(tag, value)
        }
    }
}