package com.matchapp.utils

enum class EntityTypes(val value: Int) {
    Poem(1),
    Author(2)
}