package com.matchday.utils.customview

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.matchday.R
import com.matchday.databinding.DialogLoadingBinding


class LoadingDialog : DialogFragment() {

    var binding: DialogLoadingBinding? = null

    companion object {

        private val instance: LoadingDialog? = LoadingDialog()

        fun instance(): LoadingDialog {
            if (instance == null) {
                return LoadingDialog()
            }

            return instance
        }
    }

    override fun getTheme(): Int {
        return R.style.fullScreenDialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_loading, container, false)
        dialog?.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return binding?.root
    }
}