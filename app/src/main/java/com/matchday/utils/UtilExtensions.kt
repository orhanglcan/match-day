package com.matchapp.utils

import android.content.res.Resources
import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.core.graphics.ColorUtils

val Int.dp: Int get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()


inline val @receiver:ColorInt Int.dark
    @ColorInt
    get() = ColorUtils.blendARGB(this, Color.BLACK, 0.2f)

inline val @receiver:ColorInt Int.darker
    @ColorInt
    get() = ColorUtils.blendARGB(this, Color.BLACK, 0.4f)

inline val @receiver:ColorInt Int.darkest
    @ColorInt
    get() = ColorUtils.blendARGB(this, Color.BLACK, 0.6f)


class UtilExtensions {
    companion object {
        fun intSafeArrayOf(vararg elements: Int): IntArray {
            val arrayTemp = IntArray(elements.size)

            elements.forEachIndexed { index, i ->
                arrayTemp[index] = i
            }

            return arrayTemp
        }
    }
}