package com.matchday.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import androidx.core.content.FileProvider;
import androidx.databinding.ViewDataBinding;
import androidx.exifinterface.media.ExifInterface;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.google.gson.Gson;
import com.matchday.MatchApp;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import kotlin.Unit;

public class CommonUtils {

    public static void goWhere(Activity activity, int navHost, int destination, Bundle data) {
        NavController navController = Navigation.findNavController(activity, navHost);
        navController.navigate(destination, data);
    }

    public static Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        FileOutputStream output = null;
        try {
            String path = view.getContext().getFilesDir() + "/file.png";
            output = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            output.close();
            shareImage(view.getContext(), path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

   /* public static void write(InputStream is, String DB_PATH, Unit completed) {
        try {
            OutputStream out = new FileOutputStream(new File(DB_PATH));
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = is.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            is.close();
            out.flush();
            out.close();
            completed.notify();
            System.err.println(out + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/



    public static Uri getLocalURI(View view,String filename) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        FileOutputStream output = null;
        try {
            String path = view.getContext().getFilesDir() + "/"+filename+".png";
            output = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            output.close();
            return Uri.parse(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }




    public static void shareImage(Context context, String imagePath) {
        //Uri fileUri = Uri.fromFile(new File(imagePath));

        Uri imageUri = FileProvider.getUriForFile(
                context,
                "com.matchapp.provider", //(use your app signature + ".provider" )
                new File(imagePath));

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/jpeg");
        intent.putExtra(Intent.EXTRA_STREAM, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.startActivity(Intent.createChooser(intent, "Şiiri Paylaş"));
    }

    public static int getTransparentColor(int color, float transparency) {
        int alpha = Color.alpha(color);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        alpha *= transparency;
        return Color.argb(alpha, red, green, blue);
    }

    public static void goWhere(Activity activity, int navHost, int current, int destination, Bundle data, boolean clearTop) {
        NavController navController = Navigation.findNavController(activity, navHost);
        NavOptions navOptions = new NavOptions.Builder().setPopUpTo(current, clearTop).build();
        navController.popBackStack(destination, true);
        navController.navigate(destination, data, navOptions);

    }

    public static boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) MatchApp.Companion.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        } else {
            return false;
        }
    }

    public static String getLongToDateTime(Long millis) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm", Locale.getDefault());
            return sdf.format(new Date(millis));
        } catch (Exception e) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm", Locale.getDefault());
            return sdf.format(new Date(0));
        }

    }

    public static String objectToString(Object obj) {
        return new Gson().toJson(obj);
    }

    public static int greatestCommonFactor(int width, int height) {
        return (height == 0) ? width : greatestCommonFactor(height, width % height);
    }

    public static int getScreenHeight() {
        WindowManager wm = (WindowManager) MatchApp.Companion.getInstance().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int getScreenWidth() {
        WindowManager wm = (WindowManager) MatchApp.Companion.getInstance().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static boolean isLocationEnabled() {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(MatchApp.Companion.getInstance().getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(MatchApp.Companion.getInstance().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static String getCityName(double lat, double lon) {
        Geocoder geocoder = new Geocoder(MatchApp.Companion.getInstance().getApplicationContext());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                return strReturnedAddress.toString();
            } else return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static File createImageFile(Activity activity) throws IOException {
        // Create an image file name

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage, String path)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        Log.i("getPath", path);

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(img, selectedImage, path);
        return img;
    }


    public static String getPathFromURI(Uri contentUri, Activity activity) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage, String path) throws IOException {

        ExifInterface ei = new ExifInterface(path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }


    public static boolean isTCKNCorrect(String id) {
        if (id == null) return false;
        if (id.length() != 11) return false;
        char[] chars = id.toCharArray();
        int[] a = new int[11];
        for (int i = 0; i < 10; i++) {
            a[i] = chars[i] - '0';
        }
        if (a[0] == 0) return false;
        if (a[10] % 2 == 1) return false;
        if (((a[0] + a[2] + a[4] + a[6] + a[8]) * 7 - (a[1] + a[3] + a[5] + a[7])) % 10 != a[9])
            return false;

        return (a[0] + a[1] + a[2] + a[3] + a[4] + a[5] + a[6] + a[7] + a[8] + a[9]) % 10 == a[10];
    }

    public static AlertDialog setupDialog(Context context, boolean cancelable, ViewDataBinding binding) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(binding.getRoot());
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setContentView(binding.getRoot());
        alertDialog.setCancelable(cancelable);
        return alertDialog;
    }

    public static Bitmap convertBase64ToBitmap(String b64) {

        try {
            //Log.d("baseToBitmap",b64);

            if (b64 != null && !b64.equals("null") && !b64.equals("")) {
                byte[] decodedString = Base64.decode(b64, Base64.DEFAULT);
                return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    public static String convertBitmapToB64(Bitmap bitmap) {

        try {
            if (bitmap != null) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);

                return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertSecondsToMinute(int timerCounter) {
        String result;
        String minutes = String.valueOf(Math.round(timerCounter / 60));
        String seconds = String.valueOf(timerCounter % 60);

        if (minutes.length() < 2)
            minutes = 0 + minutes;

        if (seconds.length() < 2)
            seconds = 0 + seconds;

        result = minutes + ":" + seconds;
        return result;


    }

    public static Object convertPhoneNumber(String phoneNumber) {
        String[] splitted1 = phoneNumber.split("\\(");
        String[] splitted2 = splitted1[1].split("\\)");
        String concatted1 = splitted2[0] + splitted2[1];
        String[] splitted3 = concatted1.split(" ");
        String concatted2 = splitted3[0] + splitted3[1];
        String[] splitted4 = concatted2.split("-");
        String concatted3 = splitted4[0] + splitted4[1] + splitted4[2];
        return concatted3;
    }

    public static String convertTime(String text, String pattern, String targetPattern) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(targetPattern, Locale.getDefault());
            Date date = simpleDateFormat.parse(text);
            if (date != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                return (simpleDateFormat2.format((calendar.getTime())));

            } else {
                return "";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            return "";
        }
    }


    public static String convertLocaleTime(String text) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX", Locale.getDefault());
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale.getDefault());
            Date date = simpleDateFormat.parse(text);
            if (date != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                return (simpleDateFormat2.format((calendar.getTime())));

            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertBdate(String text) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
            Date date = simpleDateFormat.parse(text);
            if (date != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                return (simpleDateFormat2.format((calendar.getTime())));

            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getLocaleTime() {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSX", Locale.getDefault());
            Date date = new Date(System.currentTimeMillis());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return (simpleDateFormat.format((calendar.getTime())));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String arrayToString(Object convertArray) {
        Gson gson = new Gson();
        return gson.toJson(convertArray);
    }

    public static String getRandomString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    public static String getRandomString(int length) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    public static String getRandomPlate() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        int randomCity = rnd.nextInt(81) + 1;
        String first = (randomCity <= 9 ? "0" + randomCity : "" + randomCity);

        StringBuilder mid = new StringBuilder();


        int last = 100 + rnd.nextInt((9999 - 100) + 1);


        while (mid.length() < (new Random().nextInt(3)) + 1) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            mid.append(SALTCHARS.charAt(index));
        }


        return (first + " " + mid.toString() + " " + last);

    }

    public static String readFileAsBase64String(String path) {
        try {
            InputStream is = new FileInputStream(path);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Base64OutputStream b64os = new Base64OutputStream(baos, Base64.DEFAULT);
            byte[] buffer = new byte[8192];
            int bytesRead;
            try {
                while ((bytesRead = is.read(buffer)) > -1) {
                    b64os.write(buffer, 0, bytesRead);
                }
                return baos.toString();
            } catch (IOException e) {
                Log.e("IMAGE_CONVERTER", "Cannot read file " + path, e);
                // Or throw if you prefer
                return "";
            } finally {
                closeQuietly(is);
                closeQuietly(b64os); // This also closes baos
            }
        } catch (FileNotFoundException e) {
            Log.e("IMAGE_CONVERTER", "File not found " + path, e);
            // Or throw if you prefer
            return "";
        }
    }

    private static void closeQuietly(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
        }
    }

}
