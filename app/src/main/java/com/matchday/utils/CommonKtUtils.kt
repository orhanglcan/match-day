package com.matchapp.utils

import android.content.res.Configuration
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.text.Spanned
import android.util.Base64
import android.util.Base64OutputStream
import android.widget.TextView
import com.matchday.MatchApp
import java.io.*
import java.util.*

class CommonKtUtils {

    companion object {
        private val instance: CommonKtUtils? = null
        fun newInstance(): CommonKtUtils {
            if (instance == null)
                return CommonKtUtils()

            return instance
        }

        fun getFieldFormat(value: String?): String {
            if (value == null && value != "null") return "-"
            else if (value == "null" || value == "") return "-"
            return value.toString()
        }

        fun getFieldFormatEmpty(value: String?): String {
            if (value == null && value != "null") return ""
            else if (value == "null") return ""
            return value.toString()
        }

        fun getFieldGender(gender: String? = ""): String? {
            when (gender?.toLowerCase(Locale.ROOT)) {
                "male" -> return "Erkek"
                "female" -> return "Kadın"
            }

            return gender
        }

        fun toHtml(data: String): Spanned? {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(data, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(data)
            }
        }

        fun textAnimation(txt: TextView, a: Char, countData: Int, appName: String, completed: () -> Unit) {
            var count = countData
            val sb = StringBuilder()
            sb.append(txt.text.toString())
            sb.append(a)
            txt.text = sb.toString()

            Handler(Looper.getMainLooper()).postDelayed({
                if (a.toString() == "y") {
                    val str = ""
                    completed()
                } else {
                    if (count < appName.length) {
                        val charArray: CharArray = appName.toCharArray()
                        val i: Int = count
                        count = i + 1
                        textAnimation(txt, charArray[i], count, appName, completed)
                    }
                }
            }, 100)
        }

        fun writeDatabase(inputStream: InputStream, DB_PATH: String, completed: () -> Unit) {
            try {
                val out = FileOutputStream(File(DB_PATH))
                var read = 0
                val bytes = ByteArray(1024)

                while (inputStream.read(bytes).let { read = it; it != -1 }) {
                    out.write(bytes, 0, read)
                }

                inputStream.close()
                out.flush()
                out.close()
                completed()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
        }

    }

    fun isDarkModeOn(): Boolean {
        val currentNightMode =
            MatchApp.instance!!.applicationContext.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        return currentNightMode == Configuration.UI_MODE_NIGHT_YES
    }

    fun convertImageFileToBase64(imageFile: File): String {
        return FileInputStream(imageFile).use { inputStream ->
            ByteArrayOutputStream().use { outputStream ->
                Base64OutputStream(outputStream, Base64.DEFAULT).use { base64FilterStream ->
                    inputStream.copyTo(base64FilterStream)
                    base64FilterStream.close() // This line is required, see comments
                    outputStream.toString()
                }
            }
        }
    }

}