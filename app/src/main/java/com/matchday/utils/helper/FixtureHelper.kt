package com.matchday.utils.helper

import com.matchday.data.api.models.Fixture
import com.matchday.data.local.entity.Team
import java.util.*


class FixtureHelper {

    fun getFixtures(
        teams: List<Team>,
        includeReverseFixtures: Boolean
    ): List<MutableList<Fixture>> {
        var numberOfTeams = teams.size

        var ghost = false
        if (numberOfTeams % 2 != 0) {
            numberOfTeams++
            ghost = true
        }

        val totalRounds = numberOfTeams - 1
        val matchesPerRound = numberOfTeams / 2
        var rounds: MutableList<MutableList<Fixture>> = LinkedList<MutableList<Fixture>>()
        for (round in 0 until totalRounds) {
            val fixtures: MutableList<Fixture> = LinkedList<Fixture>()
            for (match in 0 until matchesPerRound) {
                val home = (round + match) % (numberOfTeams - 1)
                var away = (numberOfTeams - 1 - match + round) % (numberOfTeams - 1)
                // Last team stays in the same place while the others
                // rotate around it.
                if (match == 0) {
                    away = numberOfTeams - 1
                }
                fixtures.add(Fixture(teams[home], teams[away]))
            }
            rounds.add(fixtures)
        }

        val interleaved: MutableList<MutableList<Fixture>> =
            LinkedList<MutableList<Fixture>>()
        var evn = 0
        var odd = numberOfTeams / 2
        for (i in rounds.indices) {
            if (i % 2 == 0) {
                interleaved.add(rounds[evn++])
            } else {
                interleaved.add(rounds[odd++])
            }
        }
        rounds = interleaved


        for (roundNumber in rounds.indices) {
            if (roundNumber % 2 == 1) {
                val fixture: Fixture = rounds[roundNumber][0]
                rounds[roundNumber][0] = Fixture(fixture.awayTeam, fixture.homeTeam)
            }
        }
        if (includeReverseFixtures) {
            val reverseFixtures: MutableList<MutableList<Fixture>> =
                LinkedList<MutableList<Fixture>>()
            for (round in rounds) {
                val reverseRound: MutableList<Fixture> = LinkedList<Fixture>()
                for (fixture in round) {
                    reverseRound.add(Fixture(fixture.awayTeam, fixture.homeTeam))
                }
                reverseFixtures.add(reverseRound)
            }
            rounds.addAll(reverseFixtures)
        }
        return rounds
    }
}