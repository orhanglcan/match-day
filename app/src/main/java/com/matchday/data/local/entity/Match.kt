package com.matchday.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "matches")
data class Match(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val Id: Int? = null,

    @SerializedName("name") val name:String? = "",
    @SerializedName("name2") val name2:String? = "",
    @SerializedName("name3") val name3:String? = "",
)
