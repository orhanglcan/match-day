package com.matchday.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "teams")
data class Team(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val Id: Int? = null,

    @SerializedName("team_id") val teamId:Int? = 0,
    @SerializedName("name") val name:String? = "",
    @SerializedName("code") val code:String? = "",
    @SerializedName("logo") val logo:String? = null,
    @SerializedName("country") val country:String? = "",
    @SerializedName("is_national") val isNational:Boolean?=false,
    @SerializedName("founded") val founded:Int? = 0,
    @SerializedName("venue_name") val venueName:String? = "",
    @SerializedName("venue_surface") val venueSurface:String? = "",
    @SerializedName("venue_address") val venueAddress:String? = "",
    @SerializedName("venue_city") val venueCity:String? = "",
    @SerializedName("venue_capacity") val venueCapacity:Int? = 0
)
