package com.matchday.data.local.dao

import androidx.room.*
import com.matchday.data.local.entity.Team

@Dao
interface TeamDao {
    @Query("select * from teams ORDER BY name ASC ")
    fun getAllTeams(): List<Team?>

    @Query("select COUNT(id) from teams")
    fun getTeamCount(): Int

    @Query("select * from teams where id = :id")
    fun getTeam(id: Int): Team?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTeam(entities: Team)

    @Transaction
    fun addAllTeam(entities: List<Team>) {
        entities.forEach { insertTeam(it) }
    }
}