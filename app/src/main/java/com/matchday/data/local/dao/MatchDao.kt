package com.matchday.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Transaction
import com.matchday.data.local.entity.Team

@Dao
interface MatchDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTeam(entities: Team)

    @Transaction
    fun addAllTeam(entities: List<Team>) {
        entities.forEach { insertTeam(it) }
    }

}