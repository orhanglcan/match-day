package com.matchday.data.local.repository

import com.matchday.data.local.dao.TeamDao
import com.matchday.data.local.entity.Team

class TeamRepository(private val teamDao: TeamDao) {

    suspend fun getAllTeams(): List<Team?> {
        return teamDao.getAllTeams()
    }

    suspend fun getTeamCount(): Int {
        return teamDao.getTeamCount()
    }

    suspend fun getTeam(id:Int): Team? {
        return teamDao.getTeam(id)
    }

    suspend fun addAllTeam(teamList:ArrayList<Team>) {
        return teamDao.addAllTeam(teamList)
    }

}