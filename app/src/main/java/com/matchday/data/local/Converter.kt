package com.matchday.data.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.matchday.data.api.models.Fixture

class Converter {

    @TypeConverter
    fun fixtureListToString(model: ArrayList<Fixture>?): String {
        val type = object : TypeToken<ArrayList<Fixture>>() {}.type
        return Gson().toJson(model, type)
    }

    @TypeConverter
    fun stringToFixtureList(model: String?): ArrayList<Fixture>? {
        val type = object : TypeToken<ArrayList<Fixture>>() {}.type
        return Gson().fromJson(model, type) as ArrayList<Fixture>?
    }


}