package com.matchapp.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.matchday.data.local.Converter
import com.matchday.data.local.dao.MatchDao
import com.matchday.data.local.dao.TeamDao
import com.matchday.data.local.entity.Match
import com.matchday.data.local.entity.Team

@Database(
    entities = [Match::class, Team::class],
    version = 1,
    exportSchema = false
)

@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        private const val databaseName = "matchDb"
        private var appDatabase: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            if (appDatabase == null) {
                appDatabase = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    databaseName)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigrationFrom()
                    .build()
            }

            return appDatabase as AppDatabase
        }
    }

    abstract fun teamDao(): TeamDao
    abstract fun matchDao(): MatchDao

}