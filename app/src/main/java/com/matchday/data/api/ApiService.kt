package com.matchapp.data.api

import com.matchday.data.responses.TeamApiResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path


interface ApiService {
    @GET("league/{league_id}")
    fun getTeamNames(
        @Path("league_id") id: Int
    ): Observable<Response<TeamApiResponse?>>

    @GET("teams")
    fun getWorldCupTeams(
    ): Observable<Response<Any?>>
}