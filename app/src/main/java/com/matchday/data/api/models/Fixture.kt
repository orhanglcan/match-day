package com.matchday.data.api.models

import com.matchday.data.local.entity.Team

data class Fixture(
    val homeTeam:Team,
    val awayTeam:Team,
)
