package com.matchapp.data.api

import android.content.Intent
import android.util.Log
import com.google.gson.GsonBuilder
import com.matchapp.utils.Constants
import com.matchapp.utils.manager.CacheManager
import com.matchday.BuildConfig
import com.matchday.MatchApp
import com.matchday.ui.splash.SplashActivity
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import kotlin.jvm.Throws

object ApiClient {

    private var mInstance: ApiService? = null
    private var retrofit: Retrofit? = null
    private var okHttpClient: OkHttpClient? = null

    fun <T> createRetrofitService(): T? {
        if (retrofit == null) {
            try {
                val myCache = Cache(MatchApp.instance!!.cacheDir!!, (100 * 1024 * 1024).toLong())
                val interceptor = Interceptor { chain: Interceptor.Chain ->
                    val requestBuilder = chain.request().newBuilder()
                    requestBuilder
                        //.addHeader("Content-Type", "application/json")
                        //.addHeader("Authorization", "Bearer " + Constants.TOKEN)
                        //.addHeader("Accept", "application/json")
                        //.addHeader("Expect", "application/json")
                        //.addHeader("X-Requested-With", "XMLHttpRequest")
                        //.addHeader("useQueryString", "true")
                        .addHeader("x-rapidapi-key", Constants.RAPID_API_KEY)
                        .addHeader("x-rapidapi-host", Constants.RAPID_API_HOST)
                        //.addHeader("X-Device-Key", Constants.DEVICE_ID)
                        //.addHeader("User-agent", Constants.USER_AGENT);
                    val request = requestBuilder.build()
                    chain.proceed(request)
                }
                val gson = GsonBuilder()
                    .setLenient() //.registerTypeHierarchyAdapter(Collection.class, new CollectionAdapter())
                    .create()
                okHttpClient = OkHttpClient()
                val builder = OkHttpClient.Builder()
                builder.addInterceptor(interceptor)
                try {
                    if (BuildConfig.DEBUG || BuildConfig.DEBUG) {
                        val loggingInterceptor = HttpLoggingInterceptor()
                        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                        builder.addInterceptor(loggingInterceptor)
                    }
                    if (!BuildConfig.DEBUG || BuildConfig.DEBUG) {
                        val trustAllCerts = arrayOf<TrustManager>(
                            object : X509TrustManager {
                                @Throws(CertificateException::class)
                                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                                }

                                @Throws(CertificateException::class)
                                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                                }

                                override fun getAcceptedIssuers(): Array<X509Certificate> {
                                    return arrayOf()
                                }
                            }
                        )
                        val sslContext = SSLContext.getInstance("SSL")
                        sslContext.init(null, trustAllCerts, SecureRandom())
                        val sslSocketFactory = sslContext.socketFactory
                        builder.sslSocketFactory(sslSocketFactory, (trustAllCerts[0] as X509TrustManager))
                        builder.hostnameVerifier(HostnameVerifier { hostname, session -> true })
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                builder.cache(myCache)
                builder.connectTimeout(30, TimeUnit.SECONDS)
                builder.readTimeout(30, TimeUnit.SECONDS)
                builder.writeTimeout(30, TimeUnit.SECONDS)
                okHttpClient = builder.build()
                retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient!!)
                    .build()
            } catch (e: Exception) {
                println(e)
            }
        }
        mInstance = retrofit!!.create(ApiService::class.java)

        return mInstance as T?
    }

    fun <T : Any> fetch(
        request: Observable<Response<T?>>,
        success: (response: T, header: Headers, code: Int, message: String) -> Unit,
        failure: (message: String) -> Unit,
        alert: (responseCode: Int, message: String) -> Unit
    ) {
        request.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .safeSubscribe(object : Observer<Response<T?>> {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(response: Response<T?>) {
                    when (response.code()) {
                        in 200..301 -> {
                            success(response.body() as T, response.headers(), response.code(), response.message().toString())
                        }
                        401 -> {
                            notAuthentication()
                        }
                        400, 404, 500 -> {
                            failure(response.message().toString())
                        }
                    }

                    if (response.code() != 200)
                        alert(response.code(), response.message().toString())
                }

                override fun onError(e: Throwable) {
                    failure(e.message.toString())
                    e.printStackTrace()
                }

                override fun onComplete() {
                }
            })
    }

    fun notAuthentication() {
        try {
            val context = MatchApp.instance?.applicationContext!!
            CacheManager.instance?.fullClearSP()
            context.startActivity(Intent(context, SplashActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP))
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    val service: ApiService
        get() {
            if (mInstance == null) {
                mInstance = setInstance()
            }
            return mInstance!!
        }

    private fun setInstance(): ApiService? {
        mInstance = null
        mInstance =
            createRetrofitService<ApiService>()
        return mInstance
    }
}