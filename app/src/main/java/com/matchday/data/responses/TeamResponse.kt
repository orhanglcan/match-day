package com.matchday.data.responses

import com.google.gson.annotations.SerializedName
import com.matchday.data.local.entity.Team

data class TeamApiResponse(
    @SerializedName("api") val api:TeamResponse? = null,

)

data class TeamResponse(
    @SerializedName("results") val results:Int? = 0,
    @SerializedName("teams") val teams:ArrayList<Team>? = arrayListOf(),
)


