package com.matchday.ui.main

import com.matchday.ui.bases.BaseViewModel

class MainActivityViewModel : BaseViewModel<MainNavigator>() {
    fun teamListOnClicked(){
        navigator.routeTeams()
    }
}