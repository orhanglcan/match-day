package com.matchday.ui.main

interface MainNavigator {
    fun routeTeams()
}