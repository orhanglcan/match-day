package com.matchday.ui.main

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.lifecycle.ViewModelProvider
import com.matchday.BR
import com.matchday.R
import com.matchday.databinding.ActivityMainBinding
import com.matchday.ui.bases.BaseActivity
import com.matchday.ui.teams.TeamsActivity
import com.matchday.utils.manager.ImageManager
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : BaseActivity<ActivityMainBinding,MainActivityViewModel>(), MainNavigator {

    lateinit var mainViewModel:MainActivityViewModel

    override fun getBindingVariable(): Int {
        return BR.mainVM
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getViewModel(): MainActivityViewModel {
        mainViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        return mainViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        super.onCreate(savedInstanceState)

    }
    override fun onCreated() {
        mainViewModel.navigator = this
        ImageManager.instance()?.showImage("https://fsb.zobj.net/crop.php?r=mKd-94GgWDGwSe7w2fLDJJDtc-W26k9kh3QpKMuYb0vpoMT-vKXLNhlK12Pab7nBjAxqZBsDTubaoUkjSbCRCXluUKFN4VL5NFxC7Sy5ap17VruqasLG0JSrsApBs-nsyalBy9aUviLKVHgK", viewDataBinding.imageView)





    }

    override fun routeTeams() {
        startActivity(Intent(this, TeamsActivity::class.java))
    }
}