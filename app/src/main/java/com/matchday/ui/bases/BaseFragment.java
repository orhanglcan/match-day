package com.matchday.ui.bases;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.matchapp.ui.bases.BaseCompactFragment;
import com.matchapp.utils.manager.CacheManager;

import org.jetbrains.annotations.NotNull;

public abstract class BaseFragment<T extends ViewDataBinding, V extends BaseViewModel> extends BaseCompactFragment {

    private BaseActivity mActivity;
    private View mRootView;
    private T mViewDataBinding;
    private V mViewModel;
    private View view = null;

    public abstract int getBindingVariable();

    public abstract
    @LayoutRes
    int getLayoutId();

    public abstract V getViewModel();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
        try {
            mViewModel = getViewModel();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            if (view == null) {
                mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
                mRootView = mViewDataBinding.getRoot();
                view = mRootView;
            }
            return view;
        } catch (OutOfMemoryError error) {
            Log.e("Memory ERROR", error.toString());
            return view;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.setLifecycleOwner(this);
        mViewDataBinding.executePendingBindings();
        onCreated();
    }

    public CacheManager cacheManager() {
        return CacheManager.Companion.getInstance();
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    public void onCreated() {
    }

    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }

}
