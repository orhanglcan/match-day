package com.matchapp.ui.bases

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.matchday.MatchApp
import com.matchday.data.local.repository.MatchRepository
import com.matchday.data.local.repository.TeamRepository
import javax.inject.Inject

open class BaseCompactActivity : AppCompatActivity() {
    @Inject
    lateinit var teamRepository: TeamRepository

    @Inject
    lateinit var matchRepository: MatchRepository


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        ((application) as MatchApp).getAppComponent()?.inject(this)
        //PoetyApp.instance?.getAppComponent()?.inject(this)
    }

}