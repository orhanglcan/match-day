package com.matchapp.ui.bases

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.matchday.MatchApp
import com.matchday.data.local.repository.MatchRepository
import com.matchday.data.local.repository.TeamRepository
import javax.inject.Inject

open class BaseCompactFragment : Fragment() {
    @Inject
    lateinit var teamRepository: TeamRepository

    @Inject
    lateinit var matchRepository: MatchRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ((requireActivity().application) as MatchApp).getAppComponent()?.inject(this)
    }

}