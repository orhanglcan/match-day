package com.matchday.ui.bases;

import android.view.View;

import androidx.lifecycle.ViewModel;

import java.lang.ref.WeakReference;


public abstract class BaseViewModel<N> extends ViewModel {

    private WeakReference<N> mNavigator;

    public N getNavigator() {
        if(mNavigator != null)
            return mNavigator.get();
        else return null;
    }

    public void setNavigator(N navigator) {
        this.mNavigator = new WeakReference<>(navigator);
    }

    public void onLongClicked(){}

    public void onItemClicked(View v){}

    public void onChecked(boolean isChecked){}


}
