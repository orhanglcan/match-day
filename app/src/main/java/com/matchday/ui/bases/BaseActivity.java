package com.matchday.ui.bases;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.matchapp.ui.bases.BaseCompactActivity;
import com.matchapp.utils.manager.CacheManager;
import com.matchday.BuildConfig;
import com.matchday.R;
import com.matchday.utils.customview.LoadingDialog;
import com.tapadoo.alerter.Alerter;

public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends BaseCompactActivity implements BaseFragment.Callback {

    private T mViewDataBinding;
    private V mViewModel;
    private LoadingDialog loadingDialog;
    private Boolean isCookie = false;

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    public abstract void onCreated();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = LoadingDialog.Companion.instance();
        performDataBinding();

        onCreated();
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    public CacheManager cacheManager() {
        return CacheManager.Companion.getInstance();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showLoading() {
        if (!loadingDialog.isAdded())
            loadingDialog.show(getSupportFragmentManager(), LoadingDialog.class.getName());
    }

    public void showLoadingDialog(Boolean isCancellable) {
        if (!loadingDialog.isAdded())
            loadingDialog.setCancelable(isCancellable);
        loadingDialog.show(getSupportFragmentManager(), LoadingDialog.class.getName());
    }

    public void hideLoading() {
        loadingDialog.dismiss();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        requestPermissions(permissions, requestCode);
    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
    }

    public void showAlert(String title, String message, int color) {
        try {
            if (!isCookie) {
                isCookie = true;
                Alerter.create(this)
                        .setTitle(title)
                        .setText(message)
                        .setIcon(R.drawable.ic_launcher_foreground)
                        .setBackgroundColorRes(color)
                        .setOnHideListener(() -> isCookie = false)
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAlert(String title, String message, Boolean isError) {
        try {
            if (!isCookie) {
                isCookie = true;

                int error = -1;
                if (isError) error = R.color.red_primary;
                else error = R.color.green_primary;

                Alerter.create(this)
                        .setTitle(title)
                        .setText(message)
                        .setIcon(R.drawable.ic_launcher_foreground)
                        .setBackgroundColorRes(error)
                        .setOnHideListener(() -> isCookie = false)
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showToast(String text) {
        try {
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

