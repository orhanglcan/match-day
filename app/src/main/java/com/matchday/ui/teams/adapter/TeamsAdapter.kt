package com.matchday.ui.teams.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.matchday.R
import com.matchday.data.local.entity.Team
import com.matchday.databinding.ItemTeamBinding
import java.util.*

class TeamsAdapter(teamList: List<Team?>, val onItemClick: (Team?) -> Unit) : RecyclerView.Adapter<TeamsAdapter.ViewHolder>() {

    var teamList: List<Team?> = ArrayList()
    var nowPlay = 99

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: ItemTeamBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_team,
            parent,
            false
        )
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.data = getItem(position)
    }

    private fun getItem(position: Int): Team? {
        return teamList[position]
    }

    override fun getItemCount(): Int {
        return teamList.size
    }

    inner class ViewHolder(itemView: ItemTeamBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        var binding: ItemTeamBinding = itemView
    }

    init {
        this.teamList = teamList
    }
}