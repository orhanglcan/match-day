package com.matchday.ui.teams

import com.matchday.data.local.repository.TeamRepository

interface TeamsNavigator {
    fun getTeamRepo(): TeamRepository
}