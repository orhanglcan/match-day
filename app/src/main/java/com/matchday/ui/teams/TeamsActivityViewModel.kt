package com.matchday.ui.teams

import com.matchday.data.local.entity.Team
import com.matchday.data.local.repository.TeamRepository
import com.matchday.ui.bases.BaseViewModel

class TeamsActivityViewModel : BaseViewModel<TeamsNavigator>() {

    suspend fun getTeams(): List<Team?> {
        return navigator.getTeamRepo().getAllTeams()
    }

}