package com.matchday.ui.teams

import androidx.lifecycle.ViewModelProvider
import com.matchday.BR
import com.matchday.MatchApp
import com.matchday.R
import com.matchday.data.local.entity.Team
import com.matchday.data.local.repository.TeamRepository
import com.matchday.databinding.ActivityTeamsBinding
import com.matchday.ui.bases.BaseActivity
import com.matchday.ui.teams.adapter.TeamsAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TeamsActivity : BaseActivity<ActivityTeamsBinding,TeamsActivityViewModel>(), TeamsNavigator {

    lateinit var teamsViewModel : TeamsActivityViewModel
    var teamList :List<Team?> = arrayListOf()
    var adapter:TeamsAdapter? = null

    override fun getBindingVariable(): Int {
        ((this.application) as MatchApp).getAppComponent()?.inject(this)
        return BR.teamsVM
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_teams
    }

    override fun getViewModel(): TeamsActivityViewModel {
        teamsViewModel = ViewModelProvider(this).get(TeamsActivityViewModel::class.java)
        return teamsViewModel
    }

    override fun onCreated() {
        teamsViewModel.navigator = this
        CoroutineScope(Dispatchers.Main).launch {
            val teams = teamsViewModel.getTeams()
            if(teams.isNotEmpty()){
                teamList = teams
                setupRecycle()
            }
        }
    }

    override fun getTeamRepo(): TeamRepository {
        return teamRepository
    }


    fun setupRecycle(){
        adapter = TeamsAdapter(teamList = teamList) { team ->

        }
        viewDataBinding.teamsRecycle.adapter = adapter
    }
}