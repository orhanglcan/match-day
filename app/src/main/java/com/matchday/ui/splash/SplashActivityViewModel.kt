package com.matchday.ui.splash

import com.matchapp.data.api.ApiClient
import com.matchday.data.local.entity.Team
import com.matchday.ui.bases.BaseViewModel

class SplashActivityViewModel : BaseViewModel<SplashNavigator>() {

    fun getTeams(id: Int, onSucces: (teamList:ArrayList<Team>?) -> Unit, onFailure: (message:String) -> Unit) {
        ApiClient.fetch(
            ApiClient.service.getTeamNames(id),
            success = { response, header, code, message ->
                onSucces(response.api?.teams)
            },
            failure = {
                onFailure(it)
            },
            alert = { responseCode, message ->

            })
    }


}