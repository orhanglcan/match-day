package com.matchday.ui.splash

import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.matchapp.data.api.ApiClient
import com.matchapp.utils.Coroutines
import com.matchday.BR
import com.matchday.MatchApp
import com.matchday.R
import com.matchday.databinding.ActivitySplashBinding
import com.matchday.ui.bases.BaseActivity
import com.matchday.ui.main.MainActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SplashActivity :  BaseActivity<ActivitySplashBinding,SplashActivityViewModel>(), SplashNavigator {

    lateinit var splashViewModel: SplashActivityViewModel

    override fun getBindingVariable(): Int {
        return BR.splashVM
    }

    override fun getLayoutId(): Int {
        ((this.application) as MatchApp).getAppComponent()?.inject(this)
        return R.layout.activity_splash
    }

    override fun getViewModel(): SplashActivityViewModel {
        splashViewModel = ViewModelProvider(this).get(SplashActivityViewModel::class.java)
        return splashViewModel
    }

    override fun onCreated() {
        splashViewModel.navigator = this
        CoroutineScope(Dispatchers.Main).launch {
            val totalTeam = teamRepository.getTeamCount()
            if(totalTeam == 0){
                getTeamOnAPI()
            }else{
               routeMain()
            }
        }
    }

    suspend fun getTeamOnAPI() {
        splashViewModel.getTeams(2,{ teamList->
            if( teamList != null) {
                CoroutineScope(Dispatchers.Main).launch {
                    teamRepository.addAllTeam(teamList)
                    routeMain()
                    Log.e("WRITE_DB","OK")
                }
            }
        },{
            showAlert("API", it,true)
        })
    }

    fun routeMain(){
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP))

        },2000)
    }


}