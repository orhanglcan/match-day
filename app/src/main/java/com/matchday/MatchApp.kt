package com.matchday

import android.annotation.SuppressLint
import android.provider.Settings
import androidx.multidex.MultiDexApplication
import com.matchapp.di.component.AppComponent
import com.matchapp.di.component.DaggerAppComponent
import com.matchapp.di.module.AppModule
import com.matchapp.di.module.RoomModule
import com.matchapp.utils.Constants
import com.matchday.utils.manager.ImageManager

class MatchApp : MultiDexApplication() {
    @SuppressLint("HardwareIds")
    override fun onCreate() {
        super.onCreate()

        instance = this
        val versionCode = BuildConfig.VERSION_CODE
        Constants.USER_AGENT = "MatchApp/" + versionCode + " " + System.getProperty("http.agent")
        Constants.DEVICE_ID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID).toString()
        ImageManager.instance()?.init(applicationContext)
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .roomModule(RoomModule(this))
            .build()

    }


    fun getAppComponent(): AppComponent? {
        return appComponent
    }

    companion object {
        var appComponent: AppComponent? = null

        @get:Synchronized
        var instance: MatchApp? = null
            private set
    }
}